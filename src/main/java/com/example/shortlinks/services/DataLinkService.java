package com.example.shortlinks.services;

import com.example.shortlinks.dto.DataLinkDto;
import com.example.shortlinks.exceptions_handling.ShortLinkNotFoundException;
import com.example.shortlinks.model.DataLink;
import com.example.shortlinks.repositories.DataLinkRepository;
import com.example.shortlinks.utils.ShortLinkGeneration;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DataLinkService {

    private final DataLinkRepository dataLinkRepository;

    public Optional<DataLinkDto> findDataLinkDtoById(Long id) {
        return dataLinkRepository.findById(id).map(DataLinkDto::new);
    }

    public Optional<DataLinkDto> findDataDtoLinkByShortLink(String shortLink){
        return dataLinkRepository.findDataLinkByShortLink(shortLink).map(DataLinkDto::new);
    }

    public DataLink update(DataLink dataLink) {
        if(dataLink.getId() == null) throw new ShortLinkNotFoundException("Id is null");
        return dataLinkRepository.save(dataLink);
    }

    public void deleteById(Long id) {
        DataLink dataLink = dataLinkRepository.findById(id).orElseThrow(() -> new ShortLinkNotFoundException("Short link with id: " + id + " doesn't exist"));
        dataLinkRepository.deleteById(dataLink.getId());
    }

    @Transactional
    @Scheduled(cron = "@hourly")
    public void deleteOnTimeToLive(){
        dataLinkRepository.deleteDataLinkByCreatedAtBefore(LocalDateTime.now().minusHours(24));
    }

    public DataLink save(DataLink dataLink) {
        return dataLinkRepository.findDataLinkByFullLink(dataLink.getFullLink()).orElseGet(() -> {
            dataLink.setShortLink(new ShortLinkGeneration().encode(dataLinkRepository.save(dataLink).getId()));
            return dataLinkRepository.save(dataLink);
        });
    }
}
