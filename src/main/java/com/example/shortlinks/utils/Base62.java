package com.example.shortlinks.utils;

public interface Base62 {
    String encode(Long value);
    Long decode(String link);
}
