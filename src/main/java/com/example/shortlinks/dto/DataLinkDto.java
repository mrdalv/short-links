package com.example.shortlinks.dto;

import com.example.shortlinks.model.DataLink;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DataLinkDto {

    private Long id;
    private String fullLink;
    private String shortLink;
    private Boolean isDeleted;

    public DataLinkDto(DataLink dataLink) {
        this.id = dataLink.getId();
        this.fullLink = dataLink.getFullLink();
        this.shortLink = dataLink.getShortLink();
        this.isDeleted = dataLink.getIsDeleted();
    }
}
