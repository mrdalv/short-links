package com.example.shortlinks.repositories;

import com.example.shortlinks.model.DataLink;
import org.springframework.data.jpa.repository.JpaRepository;
import java.time.LocalDateTime;
import java.util.Optional;

public interface DataLinkRepository extends JpaRepository<DataLink, Long> {
    Optional<DataLink> findDataLinkByShortLink(String shortLink);
    Optional<DataLink> findDataLinkByFullLink(String fullLink);
    void deleteDataLinkByCreatedAtBefore(LocalDateTime expireDate);
}
