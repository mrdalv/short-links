package com.example.shortlinks.controllers;

import com.example.shortlinks.dto.DataLinkDto;
import com.example.shortlinks.exceptions_handling.ShortLinkNotFoundException;
import com.example.shortlinks.model.DataLink;
import com.example.shortlinks.services.DataLinkService;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.flywaydb.core.internal.util.UrlUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

@RestController
@RequestMapping("/api/v1/shortlink")
@RequiredArgsConstructor
public class DataLinkController {

    private final MeterRegistry registry;
    private final DataLinkService dataLinkService;

    @GetMapping
    public DataLinkDto findDataLinkById(@RequestParam(value="id") Long id) {
        return dataLinkService.findDataLinkDtoById(id).orElseThrow(() -> new ShortLinkNotFoundException("Short link with id: " + id + " doesn't exist"));
    }

    @GetMapping("/{shortLink}")
    public RedirectView sendRedirect(@PathVariable String shortLink) {
        registry.counter("metrics.shortlink", "value", shortLink).increment();
        return new RedirectView(dataLinkService.findDataDtoLinkByShortLink(shortLink).orElseThrow(() -> new ShortLinkNotFoundException("Data link with short link: " + shortLink + " doesn't exist")).getFullLink());
    }

    @PostMapping
    public String saveNewDataLink(HttpServletRequest request, @RequestBody DataLink dataLink) {
        return ServletUriComponentsBuilder.fromRequestUri(request)
                .build()
                .toUriString() +
                dataLinkService.save(dataLink).getShortLink();
    }

    @PutMapping
    public DataLink updateDataLink(@RequestBody DataLink dataLink) {
        return dataLinkService.update(dataLink);
    }

    @DeleteMapping
    public void deleteDataLink(@RequestParam(value="id") Long id) {
        dataLinkService.deleteById(id);
    }
}

