package com.example.shortlinks.exceptions_handling;

import lombok.Data;
import java.util.Date;

@Data
public class DataLinkError {
    private int status;
    private String message;
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    private String debugMessage;
    private Date timestamp;

    public DataLinkError(int status, String message) {
        this.status = status;
        this.message = message;
        this.timestamp = new Date();
    }
}
