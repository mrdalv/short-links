create table data_links (
   id                      serial primary key,
   full_link               varchar(255) not null,
   short_link              varchar(255),
   is_deleted              boolean default false,
   created_at              timestamp default current_timestamp,
   updated_at              timestamp default current_timestamp
);